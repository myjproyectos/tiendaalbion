<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");

$api = 'https://www.albion-online-data.com/api/v2/stats/gold?count=1';
$inf = json_decode(file_get_contents($api), true);
echo $inf[0]['price'];
