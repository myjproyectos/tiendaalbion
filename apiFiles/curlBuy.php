<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
//Recibe
$c1 = $_POST['c1'];
$c2 = $_POST['c2'];
$c3 = $_POST['c3'];
$c4 = $_POST['c4'];
$m = $c1 == 'QUESTITEM_TOKEN_ROYAL' ? $c1 . '_' . $c2 : $c2 . '_' . $c1;
$m1 = $c3 == '' ? $m . '_LEVEL1@1' : $m . '@1';
$m2 = $c3 == '' ? $m . '_LEVEL2@2' : $m . '@2';
$m3 = $c3 == '' ? $m . '_LEVEL3@3' : $m . '@3';
$dateRecent = Date('d M Y');

$qual = $c3 != '' ? '&qualities=' . $c3 : '';

$api = 'https://www.albion-online-data.com/api/v2/stats/Prices/';
$locations = '?locations=Bridgewatch%2CFort%20Sterling%2CLymhurst%2CMartlock%2CThetford' . $qual;
$img = 'https://albiononline2d.ams3.cdn.digitaloceanspaces.com/thumbnails/orig/';

$url = $api . $m . $locations;
$url1 = $api . $m1 . $locations;
$url2 = $api . $m2 . $locations;
$url3 = $api . $m3 . $locations;

$inf = json_decode(file_get_contents($url), true);
if ($c2 != 'T1' && $c2 != 'T2' && $c2 != 'T3') {
    $inf1 = json_decode(file_get_contents($url1), true);
    $inf2 = json_decode(file_get_contents($url2), true);
    $inf3 = json_decode(file_get_contents($url3), true);
}
$price = 0;
for ($i = 0; $i < count($inf); $i++) {
    if ($price < $inf[$i]['sell_price_min'] && date('d M Y', strtotime($inf[$i]['sell_price_min_date'])) == $dateRecent) {
        $price = $inf[$i]['sell_price_min'];
        $city = $inf[$i]['city'];
        $date = $inf[$i]['sell_price_min_date'];
    }
}

if ($c2 != 'T1' && $c2 != 'T2' && $c2 != 'T3') {
    $price1 = 0;
    for ($i = 0; $i < count($inf1); $i++) {
        if ($price1 < $inf1[$i]['sell_price_min'] && date('d M Y', strtotime($inf1[$i]['sell_price_min_date'])) == $dateRecent) {
            $price1 = $inf1[$i]['sell_price_min'];
            $city1 = $inf1[$i]['city'];
            $date1 = $inf1[$i]['sell_price_min_date'];
        }
    }

    $price2 = 0;
    for ($i = 0; $i < count($inf2); $i++) {
        if ($price2 < $inf2[$i]['sell_price_min'] && date('d M Y', strtotime($inf2[$i]['sell_price_min_date'])) == $dateRecent) {
            $price2 = $inf2[$i]['sell_price_min'];
            $city2 = $inf2[$i]['city'];
            $date2 = $inf2[$i]['sell_price_min_date'];
        }
    }

    $price3 = 0;
    for ($i = 0; $i < count($inf3); $i++) {
        if ($price3 < $inf3[$i]['sell_price_min'] && date('d M Y', strtotime($inf3[$i]['sell_price_min_date'])) == $dateRecent) {
            $price3 = $inf3[$i]['sell_price_min'];
            $city3 = $inf3[$i]['city'];
            $date3 = $inf3[$i]['sell_price_min_date'];
        }
    }
}
?>

<ul class="list-group">
    <li class="list-group-item">
        <div class="media">
            <img width="95px" src="<?php echo $img . $m; ?>" class="mr-3" alt="Imágen no disponible">
            <div class="media-body">
                <strong><?php echo $city; ?></strong><!--  <span class="badge badge-secondary">Mejor opción</span> --><br>
                Precio: <?php echo $price; ?><br>
                <i>Ultima actualización: <?php echo date("d M Y H:i", strtotime($date)); ?></i> (Hora del servidor)
            </div>
        </div>
    </li>
    <?php if ($c4 == 1) { ?>
        <ul class="list-group-item showMore">
            <?php
            for ($i = 0; $i < count($inf); $i++) {
                if (date('d M Y', strtotime($inf[$i]['sell_price_min_date'])) == $dateRecent && $price != $inf[$i]['sell_price_min']) {
            ?>
                    <li class="list-group-item">
                        <div class="media">
                            <img width="65px" src="<?php echo $img . $m; ?>" class="mr-3" alt="Imágen no disponible">
                            <div class="media-body">
                                <strong><?php echo $inf[$i]['city']; ?></strong> <br>
                                Precio: <?php echo $inf[$i]['sell_price_min']; ?><br>
                                <i>Ultima actualización: <?php echo date("d M Y H:i", strtotime($inf[$i]['sell_price_min_date'])); ?></i> (Hora del servidor)
                            </div>
                        </div>
                    </li>
            <?php }
            }
            ?>
        </ul>
    <?php }
    if ($c2 != 'T1' && $c2 != 'T2' && $c2 != 'T3') {
    ?>
        <li class="list-group-item">
            <div class="media">
                <img width="95px" src="<?php echo $jpg1 = $c3 == '' ? $img . substr($m1, 0, -2) : $img . $m1; ?>" class="mr-3" alt="Imágen no disponible">
                <div class="media-body">
                    <strong><?php echo $city1; ?></strong> <!-- <span class="badge badge-success">Mejor opción</span> --><br>
                    Precio: <?php echo $price1; ?><br>
                    <i>Ultima actualización: <?php echo date("d M Y H:i", strtotime($date1)); ?></i> (Hora del servidor)
                </div>
            </div>
        </li>
        <?php if ($c4 == 1) { ?>
            <ul class="list-group-item showMore">
                <?php
                for ($i = 0; $i < count($inf1); $i++) {
                    if (date('d M Y', strtotime($inf1[$i]['sell_price_min_date'])) == $dateRecent && $price1 != $inf1[$i]['sell_price_min']) {
                ?>
                        <li class="list-group-item">
                            <div class="media">
                                <img width="65px" src="<?php echo $jpg1 = $c3 == '' ? $img . substr($m1, 0, -2) : $img . $m1; ?>" class="mr-3" alt="Imágen no disponible">
                                <div class="media-body">
                                    <strong><?php echo $inf1[$i]['city']; ?></strong> <br>
                                    Precio: <?php echo $inf1[$i]['sell_price_min']; ?><br>
                                    <i>Ultima actualización: <?php echo date("d M Y H:i", strtotime($inf1[$i]['sell_price_min_date'])); ?></i> (Hora del servidor)
                                </div>
                            </div>
                        </li>
                <?php }
                }
                ?>
            </ul>
        <?php } ?>
        <li class="list-group-item">
            <div class="media">
                <img width="95px" src="<?php echo $jpg2 = $c3 == '' ? $img . substr($m2, 0, -2) : $img . $m2; ?>" class="mr-3" alt="Imágen no disponible">
                <div class="media-body">
                    <strong><?php echo $city2; ?></strong> <!-- <span class="badge badge-primary">Mejor opción</span> --><br>
                    Precio: <?php echo $price2; ?><br>
                    <i>Ultima actualización: <?php echo date("d M Y H:i", strtotime($date2)); ?></i> (Hora del servidor)
                </div>
            </div>
        </li>
        <?php if ($c4 == 1) { ?>
            <ul class="list-group-item showMore">
                <?php
                for ($i = 0; $i < count($inf2); $i++) {
                    if (date('d M Y', strtotime($inf2[$i]['sell_price_min_date'])) == $dateRecent && $price2 != $inf2[$i]['sell_price_min']) {
                ?>
                        <li class="list-group-item">
                            <div class="media">
                                <img width="65px" src="<?php echo $jpg2 = $c3 == '' ? $img . substr($m2, 0, -2) : $img . $m2; ?>" class="mr-3" alt="Imágen no disponible">
                                <div class="media-body">
                                    <strong><?php echo $inf2[$i]['city']; ?></strong> <br>
                                    Precio: <?php echo $inf2[$i]['sell_price_min']; ?><br>
                                    <i>Ultima actualización: <?php echo date("d M Y H:i", strtotime($inf2[$i]['sell_price_min_date'])); ?></i> (Hora del servidor)
                                </div>
                            </div>
                        </li>
                <?php }
                }
                ?>
            </ul>
        <?php } ?>
        <li class="list-group-item">
            <div class="media">
                <img width="95px" src="<?php echo $jpg3 = $c3 == '' ? $img . substr($m3, 0, -2) : $img . $m3; ?>" class="mr-3" alt="Imágen no disponible">
                <div class="media-body">
                    <strong><?php echo $city3; ?></strong> <!-- <span class="badge badge-purple">Mejor opción</span> --><br>
                    Precio: <?php echo $price3; ?><br>
                    <i>Ultima actualización: <?php echo date("d M Y H:i", strtotime($date3)); ?></i> (Hora del servidor)
                </div>
            </div>
        </li>
        <?php if ($c4 == 1) { ?>
            <ul class="list-group-item showMore">
                <?php
                for ($i = 0; $i < count($inf3); $i++) {
                    if (date('d M Y', strtotime($inf3[$i]['sell_price_min_date'])) == $dateRecent && $price3 != $inf3[$i]['sell_price_min']) {
                ?>
                        <li class="list-group-item">
                            <div class="media">
                                <img width="65px" src="<?php echo $jpg3 = $c3 == '' ? $img . substr($m3, 0, -2) : $img . $m3; ?>" class="mr-3" alt="Imágen no disponible">
                                <div class="media-body">
                                    <strong><?php echo $inf3[$i]['city']; ?></strong> <br>
                                    Precio: <?php echo $inf3[$i]['sell_price_min']; ?><br>
                                    <i>Ultima actualización: <?php echo date("d M Y H:i", strtotime($inf3[$i]['sell_price_min_date'])); ?></i> (Hora del servidor)
                                </div>
                            </div>
                        </li>
                <?php }
                }
                ?>
            </ul>
    <?php }
    } ?>
</ul>