import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'minMax'
})
export class MinMaxPipe implements PipeTransform {

  transform(data: any[]): any[] {

    return data.sort((a, b) => {
      return a.sell_price_min - b.sell_price_min
    })

  }

}
