import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/api/home/home.component';
import { LevelupComponent } from './components/api/levelup/levelup.component';

const routes: Routes = [
  { path: 'Home', component: HomeComponent },
  { path: 'Level', component: LevelupComponent },
  { path: '**', redirectTo: 'Home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
