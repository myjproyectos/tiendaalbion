import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import * as options  from "./options"

@Component({
  selector: 'app-comprar',
  templateUrl: './comprar.component.html',
  styles: [
    `.form-check-input:active { filter: none; }
    .nav-tabs .nav-link.active {
      border-color: #333 #333 #fff; 
      border-bottom-right-radius: 0;
    }
  `
  ]
})
export class ComprarComponent implements OnInit {

  @Input() section: string = ''
  @Output() margin: EventEmitter<any>

  type: any[] = []; tier: any[] = []
  cdn: string = 'https://albiononline2d.ams3.cdn.digitaloceanspaces.com/thumbnails/orig/'
  _api: string = 'https://www.albion-online-data.com/api/v2/stats/Prices/'
  _location: string = '?locations=Bridgewatch%2CFort%20Sterling%2CLymhurst%2CMartlock%2CThetford%2CCaerleon'
  _type: string = ''
  _button: boolean = false; _load: boolean = false
  _t1: string = ''; _t2: string = ''
  //Arrays de informacion de la API
  _normal: any[] = []; _pcomun: any[] = []; _raro: any[] = []; _excep: any[] = []

  constructor(private http: HttpClient) {
    this.margin = new EventEmitter();
  }

  ngOnInit(): void {
    this.type = [
      { "name": "Fibra", "value": "FIBER" },
      { "name": "Madera", "value": "WOOD" },
      { "name": "Mineral", "value": "ORE" },
      { "name": "Piedra", "value": "ROCK" },
      { "name": "Piel", "value": "HIDE" },
    ]
    this.tier = options.tier
  }

  back() {
    this.margin.emit(0)
  }

  search(type: string, tier: string) {
    this._load = true
    this.http.get(this._api + tier + '_' + type + this._location)
      .subscribe((res: any) => {
        this._normal = res
        this._load = false
      })
    this.http.get(this._api + tier + '_' + type + '_LEVEL1@1' + this._location).subscribe((r: any) => this._pcomun = r)
    this.http.get(this._api + tier + '_' + type + '_LEVEL2@2' + this._location).subscribe((r: any) => this._raro = r)
    this.http.get(this._api + tier + '_' + type + '_LEVEL3@3' + this._location).subscribe((r: any) => this._excep = r)
  }

}
