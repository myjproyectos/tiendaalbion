import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  items: any[] = []
  cdn: string = ''
  margin: number = 0

  _title: string = ''
  _color: boolean = true

  constructor() { }

  ngOnInit(): void {
    this.cdn = 'https://albiononline2d.ams3.cdn.digitaloceanspaces.com/thumbnails/orig/'
    this.items = [
      {
        "title": "Recursos",
        "img" : ["T2_FIBER", "T3_WOOD", "T4_HIDE"]
      },
      {
        "title": "Material Refinado",
        "img" : ["T2_CLOTH", "T3_PLANKS", "T4_METALBAR"]
      },
      {
        "title": "Artefactos",
        "img" : ["T4_ARTEFACT_2H_HAMMER_UNDEAD", "T5_ARTEFACT_2H_TRIDENT_UNDEAD", "T6_ARTEFACT_2H_IRONGAUNTLETS_HELL"]
      },
      {
        "title": "Accesorios",
        "img" : ["T4_OFF_SHIELD", "T5_OFF_SPIKEDSHIELD_MORGANA", "T6_OFF_ORB_MORGANA"]
      },
      {
        "title": "Vestuarios",
        "img" : ["T4_HEAD_PLATE_SET1", "T5_ARMOR_PLATE_SET1", "T6_SHOES_PLATE_SET3"]
      },
      {
        "title": "Armas",
        "img" : ["T6_2H_BOW", "T7_2H_CROSSBOWLARGE", "T8_MAIN_CURSEDSTAFF"]
      }
    ]
  }

  evento(e: any, c: boolean = true){
    this._title = e.title
    this._color = c
    this.margin = -100
  }

}