import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-vender',
  templateUrl: './vender.component.html',
  styles: [
  ]
})
export class VenderComponent implements OnInit {

  @Input() section: string = ''
  @Output() margin: EventEmitter<any>

  constructor() {
    this.margin = new EventEmitter()
   }

  ngOnInit(): void {
  }

  back(){
    this.margin.emit(0)
  }

}
