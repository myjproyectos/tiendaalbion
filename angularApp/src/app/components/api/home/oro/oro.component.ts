import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-oro',
  templateUrl: './oro.component.html',
  styles: [
  ]
})
export class OroComponent implements OnInit {

  price: any = '' 

  constructor( private http: HttpClient ) { }

  ngOnInit(): void {
    this.http.get('https://www.albion-online-data.com/api/v2/stats/gold?count=1')
    .subscribe((res: any)=>{
      this.price = res[0]
    })
  }
}
