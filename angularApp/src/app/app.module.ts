import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/api/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { LevelupComponent } from './components/api/levelup/levelup.component';
import { OroComponent } from './components/api/home/oro/oro.component';
import { ComprarComponent } from './components/api/home/comprar/comprar.component';
import { VenderComponent } from './components/api/home/vender/vender.component';
import { MinMaxPipe } from './pipes/min-max.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    LevelupComponent,
    OroComponent,
    ComprarComponent,
    VenderComponent,
    MinMaxPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
